import React, { useState } from 'react';

function InlineFormStep( props ) {
  const {
    next = () => {},
    reset = () => {}
  } = props;

  function handleSubmit( event ) {
    event.preventDefault();
    next();
  }

  function handleReset( event ) {
    event.preventDefault();
    reset();
  }

  return (
    <form
      onSubmit={ handleSubmit }
      onReset={ handleReset }
    >
      { props.children }
    </form>
  );
}

function InlineForm( props ) {
  let steps;
  const { children, onSubmit, onReset } = props;
  const [ current, setCurrent ] = useState( 0 );

  function next() {
    const next = current + 1
    if ( next < steps.length )
      return setCurrent( next );

    onSubmit();
  }

  function reset() {
    onReset();
  }

  steps = React.Children.map( children, child => {
    if ( React.isValidElement( child ) )
      return React.cloneElement( child, { next, reset } );

    return child;
  } );

  return (
    <>{ steps[ current ] }</>
  );
}

export default InlineForm;

export {
  InlineFormStep
};
